import React from "react";
import Routes from "./routes";

export default class App extends React.Component {
    render() {
        return (
            <div className="App" style={{ height: "100%", width: "100%" }}>
                <Routes />
            </div>
        );
    }
}
