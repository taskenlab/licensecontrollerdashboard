import moment from "moment";

const initialState = {
    page: 0,
    rowsPerPage: 6,
    accesses: [],
    detailedAccesses: [],
    filteredDetailedAccesses: [],
    filter: {
        finalDate: moment().format("DD/MM/YYYY"),
        initialDate: moment().subtract(30, "days").format("DD/MM/YYYY"),
        selectedDate: null,
        viewType: "all",
    },
};

export default function accessInfos(state = initialState, action) {
    switch (action.type) {
        case "SET_PAGE":
            return {
                ...state,
                page: action.page,
            };
        case "SET_ROWSPERPAGE":
            return {
                ...state,
                rowsPerPage: action.rowsPerPage,
            };
        case "SET_ACCESSES":
            return {
                ...state,
                accesses: action.accesses,
            };
        case "SET_FILTER_INITIALDATE":
            return {
                ...state,
                filter: {
                    ...state.filter,
                    initialDate: action.date,
                },
            };
        case "SET_FILTER_FINALDATE":
            return {
                ...state,
                filter: {
                    ...state.filter,
                    finalDate: action.date,
                },
            };
        case "SET_FILTER_SELECTEDDATE":
            return {
                ...state,
                filter: {
                    ...state.filter,
                    selectedDate: action.date,
                },
            };
        case "SET_VIEW_TYPE":
            return {
                ...state,
                filter: {
                    ...state.filter,
                    viewType: action.viewType,
                },
            };
        case "SET_DETAILED_ACCESSES":
            return {
                ...state,
                detailedAccesses: action.detailedAccesses,
            };
        case "SET_FILTERED_DETAILED_ACCESSES":
            return {
                ...state,
                filteredDetailedAccesses: action.filteredDetailedAccesses,
            };
        default:
            return state;
    }
}
