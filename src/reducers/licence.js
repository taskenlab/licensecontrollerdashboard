const initialState = {
    licencaConfirmacao: {
        quantidade: 0,
        validade: null,
        tipoLicenca: null,
        hash: "",
    },
    licenca: {
        quantidade: 0,
        validade: null,
        tipoLicenca: null,
    },
    open: false,
};

export default function licence(state = initialState, action) {
    switch (action.type) {
        case "INSERIR_VALOR_VISUALIZAR_LICENCA": {
            return {
                ...state,
                licencaConfirmacao: {
                    quantidade: action.licenca.quantidade,
                    validade: action.licenca.validade,
                    tipoLicenca: action.licenca.tipoLicenca,
                    hash: action.licenca.hash,
                },
            };
        }
        case "INSERIR_VALOR_LICENCA": {
            return {
                ...state,
                licenca: {
                    quantidade: action.licenca.quantidade,
                    validade: action.licenca.validade,
                    tipoLicenca: action.licenca.tipoLicenca,
                },
            };
        }
        case "CHANGE_STATE_LICENCE_DIALOG": {
            return {
                ...state,
                open: action.open,
            };
        }
        default:
            return state;
    }
}
