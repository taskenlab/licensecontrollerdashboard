const initialState = {
    openChat: false,
    chatObject: {
        chatGrupo: "",
        chatRecuperador: "",
        chatPosition: {},
        chatMessages: [],
        chatCodigoRecuperador: 0,
    },
};

export default function Chat(state = initialState, action) {
    switch (action.type) {
        case "SET_CHAT_STATE": {
            return {
                ...state,
                openChat: action.openChat,
            };
        }
        case "SET_CHAT_OBJECT": {
            return {
                ...state,
                chatObject: action.chatObject,
            };
        }
        case "SET_CHAT_MESSAGES": {
            return {
                ...state,
                chatObject: {
                    ...state.chatObject,
                    chatMessages: action.messages,
                },
            };
        }
        default:
            return state;
    }
}
