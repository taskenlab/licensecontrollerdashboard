const initialState = {
    username: "",
};

export default function login(state = initialState, action) {
    switch (action.type) {
        case "SET_USERNAME":
            return {
                ...state,
                username: action.username,
            };
        default:
            return state;
    }
}
