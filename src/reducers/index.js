import { combineReducers } from "redux";
import login from "./login";
import licence from "./licence";
import accessInfos from "./accessInfos";
import chat from "./chat";

export const Reducers = combineReducers({
    login,
    licence,
    accessInfos,
    chat,
});
