import moment from "moment";
import { Store } from "../store";
import { setFilteredDetailedAccesses } from "../actions/accessInfos";

export const getDatesAndValuesBetweenTwoDates = (
    startDate,
    endDate,
    values
) => {
    var dates = [];
    var currDate = moment(startDate, "DD/MM/YYYY").startOf("day");
    var lastDate = moment(endDate, "DD/MM/YYYY").startOf("day");

    while (currDate.isSame(lastDate) || currDate.diff(lastDate, "days") < 0) {
        let accesses = values
            ? values.find((x) => x.Data === currDate.format("DD/MM/YYYY"))
            : null;

        dates.push({
            Data: currDate.format("DD/MM/YYYY"),
            Quantidade: accesses ? accesses.Quantidade : 0,
        });

        currDate.add(1, "days");
    }

    return dates.reverse();
};

export const removeNotification = (
    groupName,
    codigoRecuperador,
    filteredAccesses
) => {
    //retirando notificação
    filteredAccesses[groupName].forEach((x) => {
        if (x.CodigoRecuperador === codigoRecuperador) {
            x.Mensagens = 0;
        }
    });
    Store.dispatch(setFilteredDetailedAccesses(filteredAccesses));
    //retirando notificação
};
