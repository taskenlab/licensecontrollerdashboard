export const setValorVisualizarLicenca = (licenca) => {
    return {
        type: "INSERIR_VALOR_VISUALIZAR_LICENCA",
        licenca,
    };
};

export const setStateLicenceDialog = (open) => {
    return {
        type: "CHANGE_STATE_LICENCE_DIALOG",
        open,
    };
};

export const setValorLicenca = (licenca) => {
    return {
        type: "INSERIR_VALOR_LICENCA",
        licenca,
    };
};
