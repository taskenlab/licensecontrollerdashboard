export const setPage = (page) => {
    return {
        type: "SET_PAGE",
        page,
    };
};

export const setRowsPerPage = (rowsPerPage) => {
    return {
        type: "SET_ROWSPERPAGE",
        rowsPerPage,
    };
};

export const setAccesses = (accesses) => {
    return {
        type: "SET_ACCESSES",
        accesses,
    };
};

const dateTypes = [
    "SET_FILTER_INITIALDATE",
    "SET_FILTER_FINALDATE",
    "SET_FILTER_SELECTEDDATE",
];
export const setFilterDate = (date, type) => {
    if (!dateTypes.includes(type)) {
        return { type: "" };
    }

    return {
        type,
        date,
    };
};

export const setViewType = (viewType) => {
    return {
        type: "SET_VIEW_TYPE",
        viewType,
    };
};

export const setDetailedAccesses = (detailedAccesses) => {
    return {
        type: "SET_DETAILED_ACCESSES",
        detailedAccesses,
    };
};

export const setFilteredDetailedAccesses = (filteredDetailedAccesses) => {
    return {
        type: "SET_FILTERED_DETAILED_ACCESSES",
        filteredDetailedAccesses,
    };
};
