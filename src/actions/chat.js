export const setOpenChat = (openChat) => {
    return {
        type: "SET_CHAT_STATE",
        openChat,
    };
};

export const setChatObject = (chatObject) => {
    return {
        type: "SET_CHAT_OBJECT",
        chatObject,
    };
};

export const setChatMessages = (messages) => {
    return {
        type: "SET_CHAT_MESSAGES",
        messages,
    };
};
