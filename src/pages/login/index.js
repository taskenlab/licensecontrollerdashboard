import React from "react";
import {
    Grid,
    CardMedia,
    Button,
    IconButton,
    OutlinedInput,
    TextField,
    InputAdornment,
} from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { GridRootRow, useStyles, RightPaperLoginForm } from "./styles";
import { showErrorToast } from "../../utils/toasty";
import "react-toastify/dist/ReactToastify.css";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { login } from "../../services/auth";
import { ToastContainer } from "react-toastify";
import api from "../../services/api";
import md5 from "md5";
import qs from "querystring";
import { setUsername } from "../../actions/login";

function Login({ username, dispatch }) {
    const classes = useStyles();
    const history = useHistory();
    const [values, setValues] = React.useState({
        password: "",
        showPassword: false,
        errorUsername: false,
        errorPassword: false,
    });

    const handleChange = (props) => (event) => {
        setValues({
            ...values,
            [props[0]]: event.target.value,
            [props[1]]: false,
        });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleKeyDown = (event) => {
        if (event.key === "Enter") {
            handleClickLoginButton();
        }
    };

    const handleClickLoginButton = () => {
        if (username.trim() === "") {
            setValues({ ...values, errorUsername: true });
            return;
        }

        if (values.password.trim() === "") {
            setValues({ ...values, errorPassword: true });
            return;
        }

        api.post(
            "logar",
            qs.stringify({
                grant_type: "password",
                username: username,
                password: md5(values.password),
                from: "PANEL",
            }),
            {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
            }
        )
            .then((response) => {
                if (response.response && response.response.status === 400) {
                    showErrorToast(
                        response.response && response.response.data
                            ? "Não foi possível logar: " +
                                  response.response.data.error_description
                            : "Ocorreu um erro"
                    );
                } else {
                    login(response.data.access_token);
                    history.push("/dashboard");
                }
            })
            .catch((err) => {
                console.log(err);
                showErrorToast(
                    err.response && err.response.data
                        ? "Ocorreu um erro: " +
                              err.response.data.error_description
                        : "Ocorreu um erro"
                );
            });
    };

    return (
        <GridRootRow container direction="row" alignItems="center">
            <ToastContainer />
            <Grid
                item
                container
                xs={12}
                sm={5}
                direction="column"
                justify="center"
                alignItems="flex-end"
                className={classes.leftgrid}
            >
                <CardMedia image={"39.jpg"} className={classes.leftcardmedia}>
                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        className={classes.gridinsidecardmedia}
                    >
                        <img
                            src={"LogoTasken.svg"}
                            className={classes.logo}
                            alt=""
                        />
                        <hr className={classes.linelogo} />
                        <p className={classes.textocontrole}>
                            CONTROLE DE LICENÇAS
                        </p>
                    </Grid>
                </CardMedia>
            </Grid>
            <Grid
                item
                container
                xs={12}
                sm={7}
                direction="column"
                justify="center"
                className={classes.rightgrid}
            >
                <RightPaperLoginForm>
                    <Grid style={{ width: "80%", height: "80%" }}>
                        <p className={classes.textologin}>LogIn</p>
                        <p className={classes.labelsinput}>Nome de usuário:</p>
                        <TextField
                            variant="outlined"
                            fullWidth
                            className={classes.logininput}
                            onChange={(event) => {
                                dispatch(setUsername(event.target.value));
                                setValues({ ...values, errorUsername: false });
                            }}
                            error={values.errorUsername}
                        />
                        <p className={classes.labelsinput}>Senha:</p>
                        <OutlinedInput
                            variant="outlined"
                            type={values.showPassword ? "text" : "password"}
                            fullWidth
                            onChange={handleChange([
                                "password",
                                "errorPassword",
                            ])}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                    >
                                        {values.showPassword ? (
                                            <Visibility />
                                        ) : (
                                            <VisibilityOff />
                                        )}
                                    </IconButton>
                                </InputAdornment>
                            }
                            className={classes.passwordinput}
                            error={values.errorPassword}
                            onKeyDown={handleKeyDown}
                        />
                        {/* <p className={classes.labelesqueceusenha}>Esqueceu a senha?</p> */}
                        <Button
                            variant="contained"
                            fullWidth
                            className={classes.buttonlogin}
                            onClick={handleClickLoginButton}
                        >
                            Entrar
                        </Button>
                    </Grid>
                </RightPaperLoginForm>
            </Grid>
        </GridRootRow>
    );
}

export default connect((state) => ({ username: state.login.username }))(Login);
