import { styled, makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

export const GridRootRow = styled(Grid)({
    width: "100%",
    height: "100vh",
    margin: 0,
});

export const RightPaperLoginForm = styled(Paper)({
    display: "flex",
    height: "60%",
    width: "45%",
    borderRadius: "0px 4px 4px 0px",
    boxShadow: "0px 30px 60px rgba(0, 59, 104, 0.5)",
    justifyContent: "center",
    alignItems: "center",
});

export const useStyles = makeStyles((theme) => ({
    leftgrid: {
        display: "flex",
        background:
            "linear-gradient(180deg, #1A75BA 0%, rgba(26, 117, 186, 0.83) 100%)",
        mixBlendMode: "normal",
        width: "100%",
        height: "100%",
        margin: 0,
    },
    rightgrid: {
        display: "flex",
        background: "linear-gradient(180deg, #1A75BA 0%, #3F9DE3 100%)",
        mixBlendMode: "normal",
        width: "100%",
        height: "100%",
        margin: 0,
    },
    leftcardmedia: {
        width: "35%",
        height: "60%",
        boxShadow: "0px 30px 60px rgba(0, 59, 104, 0.5)",
        borderRadius: "4px 0px 0px 4px",
    },
    gridinsidecardmedia: {
        display: "flex",
        backgroundColor: "black",
        opacity: "0.85",
        height: "100%",
        width: "100%",
        borderRadius: "4px 0px 0px 4px",
    },
    textocontrole: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: "regular",
        fontSize: "16px",
        lineHeight: "25px",
        textAlign: "center",
        color: "#FFFFFF",
    },
    linelogo: {
        width: "60%",
        borderBottom: 0,
        borderLeft: 0,
        borderRight: 0,
    },
    logo: {
        marginBottom: 25,
        opacity: 1,
        width: "60%",
    },
    textologin: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: "24px",
        lineHeight: "32px",
        textAlign: "center",
        marginBottom: 50,
    },
    labelsinput: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "14px",
        lineHeight: "18px",
        textAlign: "left",
    },
    labelesqueceusenha: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "12px",
        lineHeight: "15px",
        textAlign: "right",
        color: "#13B1D1",
        marginBottom: 35,
    },
    logininput: {
        marginBottom: 20,
        "& input:valid:focus + fieldset": {
            borderColor: "#1A70BA",
        },
    },
    buttonlogin: {
        backgroundColor: "#1A75BA",
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "16px",
        lineHeight: "20px",
        color: "#FFFFFF",
        textTransform: "none",
        height: "50px",
        "&:hover": {
            backgroundColor: "#1A70BA",
            borderColor: "#1A70BA",
        },
    },
    passwordinput: {
        "&.Mui-focused fieldset": {
            borderColor: "#1A70BA !important",
        },
        marginBottom: 55,
    },
}));
