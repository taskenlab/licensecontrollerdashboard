import React from "react";
import { Grid } from "@material-ui/core";
import { useStyles } from "./styles";
import { connect } from "react-redux";
import HeaderDashboard from "./components/headerDashboard";
import { ToastContainer } from "react-toastify";
import AccessTable from "./components/accessTable";
import AccessChart from "./components/accessChart";
import DateFilters from "./components/dateFilters";
import DetailedAccesses from "./components/detailedAccesses";

function Dashboard({ selectedDate }) {
    const classes = useStyles();
    return (
        <Grid>
            <ToastContainer />
            <HeaderDashboard />
            <Grid
                container
                direction="column"
                alignItems="center"
                style={{ width: "100%", display: "flex", marginTop: "40px" }}
            >
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                    style={{ width: "70%", display: "flex" }}
                >
                    <Grid
                        container
                        item
                        xs={4}
                        direction="row"
                        justify="flex-start"
                        style={{ display: "flex" }}
                    >
                        <div className={classes.textoTituloLicenca}>
                            Licenças
                        </div>
                    </Grid>
                    <Grid item xs={8}>
                        <DateFilters />
                    </Grid>
                </Grid>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="center"
                    style={{
                        width: "70%",
                        display: "flex",
                        marginTop: "10px",
                        minHeight: "500px",
                    }}
                >
                    <Grid item xs={4} style={{ minWidth: "310px" }}>
                        <Grid style={{ margin: "10px 10px 10px 10px" }}>
                            <AccessTable />
                        </Grid>
                    </Grid>
                    <Grid item xs={8} style={{ minWidth: "400px" }}>
                        <Grid
                            style={{
                                border: "1px solid #DBE5ED",
                                boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
                                borderRadius: "4px",
                                margin: "15px 10px 15px 10px",
                            }}
                        >
                            <AccessChart />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {selectedDate != null ? <DetailedAccesses /> : ""}
        </Grid>
    );
}

export default connect((state) => ({
    selectedDate: state.accessInfos.filter.selectedDate,
}))(Dashboard);
