import { makeStyles, withStyles } from "@material-ui/core/styles";
import { createMuiTheme, TextField, Paper } from "@material-ui/core";

export const PaperAccessesTable = withStyles({
    root: {
        boxShadow: "0px 0px 0px 0px",
        borderRadius: "0px",
        maxHeight: 500,
    },
})(Paper);

export const DatePickerTheme = createMuiTheme({
    palette: {
        primary: {
            main: "#1A70BA",
        },
    },
    overrides: {
        MuiOutlinedInput: {
            borderColor: "#1A70BA",
        },
        MuiPickersDay: {
            daySelected: {
                backgroundColor: "#1A75BA",
            },
            current: {
                color: "#1A70BA",
            },
        },
    },
});

export const SearchTextBox = withStyles({
    root: {
        width: "70%",
        marginTop: "20px",
        marginLeft: "20px",
        "& label.Mui-focused": {
            color: "#1A70BA",
        },
        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "#CED4DA",
                borderRadius: "0px !important",
            },
            "&:hover fieldset": {
                borderColor: "black",
            },
            "&.Mui-focused fieldset": {
                borderColor: "#1A70BA",
            },
        },
    },
})(TextField);

export const useStyles = makeStyles((theme) => ({
    paginationRootDiv: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
    logo: {
        width: "60%",
    },
    textotitulo: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: "46px",
        lineHeight: "70px",
        textAlign: "center",
        color: "white",
        textShadow: "0px 2px 2px rgba(0, 0, 0, 0.25)",
    },
    gridtextinfo: {
        minWidth: "200px",
    },
    griddatainfo: {
        minWidth: "258px",
    },
    griditeminfos: {
        display: "flex",
        width: "100%",
        height: "100%",
        marginTop: "5px",
        marginBottom: "5px",
    },
    gridcircle: {
        display: "flex",
        width: "44px",
        height: "44px",
        minWidth: "44px",
        minHeight: "44px",
        backgroundColor: "#1A75BA",
        borderRadius: "25px",
    },
    textoinfo: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "18px",
        marginLeft: "10px",
    },
    textodadosinfo: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: "28px",
        lineHeight: "35px",
        marginLeft: "10px",
    },
    titulolicenca: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "16px",
        lineHeight: "20px",
        marginLeft: "10px",
    },
    tipolicenca: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "12px",
        lineHeight: "15px",
        marginLeft: "10px",
    },
    verticallinelicenca: {
        borderRight: "1px solid #DBE5ED",
        height: "36px",
    },
    verticallinedata: {
        borderLeft: "1px solid #DBE5ED",
        height: "64px",
    },
    imgHeader: {
        width: "100%",
        height: "100%",
        minHeight: "500px",
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
    },
    gridLogo: {
        display: "flex",
        backgroundColor: "white",
        width: "200px",
        height: "90px",
    },
    topLine: {
        display: "flex",
        width: "100%",
        height: "60px",
        backgroundColor: "#2B2B2B",
    },
    imageInfoGrid: {
        display: "flex",
        width: "70%",
        minHeight: "144px",
        backgroundColor: "white",
        marginBottom: "5px",
    },
    buttonArqLicenca: {
        backgroundColor: "#1A75BA",
        borderRadius: 0,
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "16px",
        lineHeight: "20px",
        color: "#FFFFFF",
        textTransform: "none",
        height: "50px",
        width: "85%",
        alignSelf: "center",
        "&:hover": {
            backgroundColor: "#1A70BA",
            borderColor: "#1A70BA",
        },
    },
    textoTituloLicenca: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "36px",
        lineHeight: "45px",
    },
    textoSair: {
        color: "#FFFFFF !important",
        marginLeft: "10px",
        marginRight: "10px",
        cursor: "pointer",
    },
    textoUsuario: {
        display: "flex",
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "18px",
        color: "#A3A3A3",
        height: "60px",
        marginTop: "0px",
        marginLeft: "5px",
        alignItems: "center",
    },
    accessTable: {
        border: "1px solid #DBE5ED",
        boxSizing: "border-box",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
        borderRadius: "4px",
        minWidth: 200,
    },
    tableRow: {
        cursor: "pointer",
        "&:hover": {
            background: "#1A75BA !important",
        },
    },
    gridDetailedAccesses: {
        width: "100%",
        display: "flex",
        marginTop: "40px",
        background: "#F6F7F9",
    },
    buttonExportar: {
        borderColor: "#0961A3",
        color: "#1A75BA",
        height: "44px",
        borderRadius: "0px",
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "16px",
        textTransform: "none",
        lineHeight: "20px",
        "&:hover": {
            color: "#1A75BA !important",
        },
    },
    buttonVisualizar: {
        marginTop: "20px",
        height: "40px !important",
    },
    selectedButtonVisualizar: {
        background: "#1A75BA",
        color: "white",
        "&:hover": {
            background: "#1A75BA",
            color: "white !important",
        },
    },
    actionButtons: {
        width: "40px",
        height: "36px",
        minWidth: "36px",
        borderRadius: "0px",
        marginRight: "10px",
    },
    notificationBadge: {
        position: "absolute",
        top: "-7px",
        right: "-10px",
        width: "20px",
        height: "20px",
        borderRadius: "50%",
        backgroundColor: "#C91F1F",
        color: "white",
    },
    downloadButton: {
        display: "none",
    },
}));
