import React, { useEffect, useRef, useState } from "react";
import { Grid } from "@material-ui/core";
import { connect } from "react-redux";
import { useStyles, SearchTextBox } from "../styles";
import { Button, InputAdornment } from "@material-ui/core";
import { AssignmentReturned, Search } from "@material-ui/icons";
import {
    setViewType,
    setDetailedAccesses,
    setFilteredDetailedAccesses,
} from "../../../actions/accessInfos";
import { setOpenChat } from "../../../actions/chat";
import DetailedAccessesTable from "./detailedAccessesTable";
import clsx from "clsx";
import moment from "moment";
import api from "../../../services/api";
import { showErrorToast } from "../../../utils/toasty";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import ExportTable from "./exportTable";
import { removeNotification } from "../../../utils/utils";

function DetailedAccesses({
    selectedDate,
    viewType,
    detailedAccesses,
    chatObject,
    filteredAccesses,
    dispatch,
}) {
    const classes = useStyles();
    const exportExcel = useRef();
    const [mustUpdate, setMustUpdate] = useState(true);
    const [loginFilter, setLoginFilter] = useState("");

    //timer para atualizar a tabela
    useEffect(() => {
        const intervalId = setInterval(() => {
            setMustUpdate(true);
        }, 30000);

        return () => clearInterval(intervalId);
    }, []);

    //escolheu outra data
    useEffect(() => {
        setLoginFilter("");
        setMustUpdate(true);
        dispatch(setOpenChat(false));
        dispatch(setViewType("all"));
    }, [selectedDate]);

    //update dos acessos a cada 30 seg ou escolha de data
    useEffect(() => {
        if (mustUpdate) {
            getDetails();
            setMustUpdate(false);
            if (chatObject.openChat) {
                removeNotification(
                    chatObject.chatGrupo,
                    chatObject.chatCodigoRecuperador,
                    filteredAccesses
                );
            }
        }
    }, [mustUpdate]);

    //update na lista de filtrados toda vez que a lista original for alterada
    useEffect(() => {
        dispatch(setFilteredDetailedAccesses(detailedAccesses));
        filterInfos(loginFilter);
    }, [detailedAccesses, dispatch]);

    useEffect(() => {
        filterInfos(loginFilter);
    }, [viewType]);

    const getDetails = () => {
        api.get("api/filtroacesso/obteracessospordata", {
            params: {
                data: selectedDate,
            },
        })
            .then((response) => {
                dispatch(setDetailedAccesses(response.data));
            })
            .catch((error) => {
                showErrorToast();
            });
    };

    const handleClickButtonVisualizar = (type) => {
        dispatch(setViewType(type));
    };

    const handleLoginFilterChange = (event) => {
        setLoginFilter(event.target.value);
        filterInfos(event.target.value);
    };

    const filterInfos = (filter) => {
        if (filter.trim() === "" && viewType === "all") {
            dispatch(setFilteredDetailedAccesses(detailedAccesses));
            return;
        }

        let filterAux = {};
        Object.keys(detailedAccesses).forEach((value) => {
            let aux = detailedAccesses[value].filter((x) => {
                return (
                    x.LoginRecuperador.toLowerCase().startsWith(
                        filter.toLowerCase()
                    ) && (viewType === "online" ? x.HorarioLogout === "" : true)
                );
            });

            if (aux.length > 0) {
                filterAux = {
                    ...filterAux,
                    [value]: aux,
                };
            }
        });

        dispatch(setFilteredDetailedAccesses(filterAux));
    };

    const handleClickButtonExportTable = () => {
        exportExcel.current.handleDownload();
    };

    return (
        <Grid
            container
            id="detailedAccesses"
            direction="column"
            alignItems="center"
            className={classes.gridDetailedAccesses}
        >
            <Grid container direction="row" justify="center">
                <img src={"meiolosango.png"} alt="" />
            </Grid>
            <Grid
                container
                direction="row"
                justify="space-between"
                style={{ width: "70%", display: "flex", marginTop: "40px" }}
            >
                <Grid
                    container
                    item
                    xs={6}
                    direction="row"
                    justify="flex-start"
                    style={{ display: "flex" }}
                >
                    <div className={classes.textoTituloLicenca}>
                        Usuários em {selectedDate}
                    </div>
                </Grid>
                <Grid container item xs={6} direction="row" justify="flex-end">
                    <ReactHTMLTableToExcel
                        className={classes.downloadButton}
                        table="acessos-xls"
                        filename="Acessos"
                        sheet="Sheet"
                        id="downloadexcel"
                        ref={exportExcel}
                    />
                    <Button
                        variant="outlined"
                        className={classes.buttonExportar}
                        onClick={handleClickButtonExportTable}
                    >
                        <AssignmentReturned
                            style={{ color: "#1A75BA", marginRight: "5px" }}
                        />
                        Exportar Tabela
                    </Button>
                </Grid>
            </Grid>
            <Grid
                style={{
                    marginTop: "40px",
                    width: "70%",
                    marginBottom: "40px",
                    border: "1px solid #DBE5ED",
                    boxSizing: "border-box",
                    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
                    borderRadius: "4px",
                }}
            >
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    style={{
                        display: "flex",
                        backgroundColor: "white",
                        height: "84px",
                    }}
                >
                    <Grid item xs={6}>
                        <SearchTextBox
                            label="Buscar por login"
                            variant="outlined"
                            margin="dense"
                            value={loginFilter}
                            onChange={handleLoginFilterChange}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Search
                                            style={{
                                                color: "rgba(0, 0, 0, 0.54)",
                                            }}
                                        />
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </Grid>
                    <Grid
                        item
                        xs={6}
                        container
                        direction="row"
                        justify="flex-end"
                        style={{
                            display: "flex",
                        }}
                    >
                        {moment().format("DD/MM/YYYY") === selectedDate && (
                            <React.Fragment>
                                <Button
                                    variant="outlined"
                                    className={clsx(
                                        classes.buttonExportar,
                                        classes.buttonVisualizar,
                                        viewType === "online"
                                            ? classes.selectedButtonVisualizar
                                            : null
                                    )}
                                    style={{ width: "150px" }}
                                    onClick={() =>
                                        handleClickButtonVisualizar("online")
                                    }
                                >
                                    Usuários Online
                                </Button>
                                <Button
                                    variant="outlined"
                                    className={clsx(
                                        classes.buttonExportar,
                                        classes.buttonVisualizar,
                                        viewType === "all"
                                            ? classes.selectedButtonVisualizar
                                            : null
                                    )}
                                    onClick={() =>
                                        handleClickButtonVisualizar("all")
                                    }
                                    style={{
                                        width: "112px",
                                        marginRight: "20px",
                                        borderLeft: "0px",
                                    }}
                                >
                                    Ver Todos
                                </Button>
                            </React.Fragment>
                        )}
                    </Grid>
                </Grid>
                <DetailedAccessesTable />
                <ExportTable />
            </Grid>
        </Grid>
    );
}

export default connect((state) => ({
    selectedDate: state.accessInfos.filter.selectedDate,
    viewType: state.accessInfos.filter.viewType,
    loginFilter: state.accessInfos.filter.loginFilter,
    detailedAccesses: state.accessInfos.detailedAccesses,
    chatObject: state.chat.chatObject,
    filteredAccesses: state.accessInfos.filteredDetailedAccesses,
}))(DetailedAccesses);
