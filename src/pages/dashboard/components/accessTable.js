import React, { useState, useRef } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import Paper from "@material-ui/core/Paper";
import { connect } from "react-redux";
import {
    setPage,
    setRowsPerPage,
    setFilterDate,
} from "../../../actions/accessInfos";
import AccessTablePaginationActions from "./accessTablePaginationActions";
import { useStyles } from "../styles";
import { HashLink as Link } from "react-router-hash-link";

function AccessTable({ page, rowsPerPage, rows, dispatch }) {
    const classes = useStyles();
    const [selectedRow, setSelectedRow] = useState(null);
    const refLink = useRef(null);

    const emptyRows =
        rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    const getPageArray = () => {
        return rowsPerPage > 0
            ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : rows;
    };

    const handleChangePage = (event, newPage) => {
        dispatch(setPage(newPage));
    };

    const handleChangeRowsPerPage = (event) => {
        dispatch(setRowsPerPage(parseInt(event.target.value, 6)));
        dispatch(setPage(0));
    };

    const handleTableRowClick = (event, row) => {
        if (selectedRow && selectedRow.Data === row.Data) {
            selectedRow.selected = false;
            setSelectedRow(null);
            dispatch(setFilterDate(null, "SET_FILTER_SELECTEDDATE"));
            return;
        }

        if (selectedRow) selectedRow.selected = false;
        row.selected = true;
        setSelectedRow(row);
        dispatch(setFilterDate(row.Data, "SET_FILTER_SELECTEDDATE"));
        refLink.current.click();
    };

    return (
        <React.Fragment>
            <Link
                to="/dashboard#detailedAccesses"
                innerRef={refLink}
                scroll={(el) =>
                    el.scrollIntoView({
                        behavior: "smooth",
                        block: "end",
                    })
                }
            ></Link>
            <TableContainer component={Paper} className={classes.accessTable}>
                <Table aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Data</TableCell>
                            <TableCell align="center">Quantidade</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {getPageArray().map((row, index) => (
                            <TableRow
                                key={index}
                                style={{
                                    background: row.selected
                                        ? "#1A75BA"
                                        : index % 2
                                        ? "#F8F8F8"
                                        : "white",
                                }}
                                hover
                                className={classes.tableRow}
                                onClick={(event) =>
                                    handleTableRowClick(event, row)
                                }
                            >
                                <TableCell component="th" scope="row">
                                    {row.Data}
                                </TableCell>
                                <TableCell
                                    style={{ width: "100%" }}
                                    align="center"
                                >
                                    {row.Quantidade}
                                </TableCell>
                            </TableRow>
                        ))}

                        {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[6]}
                                colSpan={3}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: {
                                        "aria-label": "Linhas por página",
                                    },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={AccessTablePaginationActions}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </React.Fragment>
    );
}

export default connect((state) => ({
    page: state.accessInfos.page,
    rowsPerPage: state.accessInfos.rowsPerPage,
    rows: state.accessInfos.accesses,
}))(AccessTable);
