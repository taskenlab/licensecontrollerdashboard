import React, { useEffect } from "react";
import { connect } from "react-redux";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";
import MomentUtils from "@date-io/moment";
import "moment/locale/pt-br";
import api from "../../../services/api";
import { Grid } from "@material-ui/core";
import { showErrorToast } from "../../../utils/toasty";
import { setAccesses, setFilterDate } from "../../../actions/accessInfos";
import { getDatesAndValuesBetweenTwoDates } from "../../../utils/utils";
import { DatePickerTheme } from "../styles";
import { ThemeProvider } from "@material-ui/styles";
import { setOpenChat } from "../../../actions/chat";
moment.locale("pt-br");

function DateFilters({ finalDate, initialDate, dispatch }) {
    useEffect(() => {
        api.get("api/filtroacesso/obterquantidadeacessos", {
            params: {
                dataFinal: finalDate,
                dataInicial: initialDate,
            },
        })
            .then((response) => {
                dispatch(
                    setAccesses(
                        getDatesAndValuesBetweenTwoDates(
                            initialDate,
                            finalDate,
                            response.data
                        )
                    )
                );
            })
            .catch((error) => {
                showErrorToast();
            });
    }, [finalDate, initialDate, dispatch]);

    const handleDateChange = (date, type) => {
        dispatch(setFilterDate(date ? date.format("DD/MM/YYYY") : "", type));
        dispatch(setOpenChat(false));
        dispatch(setFilterDate(null, "SET_FILTER_SELECTEDDATE"));
    };

    return (
        <Grid
            container
            direction="row"
            justify="flex-end"
            style={{ display: "flex" }}
        >
            <ThemeProvider theme={DatePickerTheme}>
                <div>
                    <MuiPickersUtilsProvider
                        libInstance={moment}
                        utils={MomentUtils}
                    >
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="DD/MM/YYYY"
                            margin="normal"
                            label="Data inicial"
                            disableFuture={true}
                            autoOk={true}
                            value={moment(initialDate, "DD/MM/YYYY")}
                            onChange={(date) =>
                                handleDateChange(date, "SET_FILTER_INITIALDATE")
                            }
                            inputVariant="outlined"
                            KeyboardButtonProps={{
                                "aria-label": "change date",
                            }}
                            invalidDateMessage="Formato de data inválido"
                            maxDateMessage="Data excedeu a data máxima"
                            minDateMessage="Data excedeu a data mínima"
                        />
                    </MuiPickersUtilsProvider>
                </div>
                <div style={{ marginLeft: "5px" }}>
                    <MuiPickersUtilsProvider
                        libInstance={moment}
                        utils={MomentUtils}
                    >
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="DD/MM/YYYY"
                            margin="normal"
                            label="Data final"
                            disableFuture={true}
                            autoOk={true}
                            value={moment(finalDate, "DD/MM/YYYY")}
                            onChange={(date) =>
                                handleDateChange(date, "SET_FILTER_FINALDATE")
                            }
                            inputVariant="outlined"
                            KeyboardButtonProps={{
                                "aria-label": "change date",
                            }}
                            invalidDateMessage="Formato de data inválido"
                            maxDateMessage="Data excedeu a data máxima"
                            minDateMessage="Data excedeu a data mínima"
                        />
                    </MuiPickersUtilsProvider>
                </div>
            </ThemeProvider>
        </Grid>
    );
}

export default connect((state) => ({
    initialDate: state.accessInfos.filter.initialDate,
    finalDate: state.accessInfos.filter.finalDate,
}))(DateFilters);
