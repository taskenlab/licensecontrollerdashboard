import React from "react";
import { connect } from "react-redux";
import { Line } from "react-chartjs-2";

function AccessChart({ accesses, dispatch }) {
    return (
        <Line
            data={{
                labels: accesses
                    .map((element) => {
                        return element.Data;
                    })
                    .reverse(),
                datasets: [
                    {
                        backgroundColor: "rgba(17,186,169, .2)",
                        borderColor: "rgb(17,186,169)",
                        data: accesses
                            .map((element) => {
                                return element.Quantidade;
                            })
                            .reverse(),
                        label: "Acessos",
                        fill: "start",
                    },
                ],
            }}
            options={{
                showLines: true,
                elements: {
                    line: {
                        tension: 0.000001,
                    },
                },
                spanGaps: false,
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                                callback: function (value) {
                                    if (Number.isInteger(value)) {
                                        return value;
                                    }
                                },
                                stepSize: 1,
                            },
                        },
                    ],
                    xAxes: [
                        {
                            ticks: {
                                autoSkip: true,
                                maxTicksLimit: 9,
                            },
                        },
                    ],
                },
            }}
        />
    );
}

export default connect((state) => ({ accesses: state.accessInfos.accesses }))(
    AccessChart
);
