import React, { useState } from "react";
import PropTypes from "prop-types";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import LocalPostOfficeIcon from "@material-ui/icons/LocalPostOffice";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import { Button } from "@material-ui/core";
import { useStyles } from "../styles";
import moment from "moment";
import api from "../../../services/api";
import { showSuccessToast, showErrorToast } from "../../../utils/toasty";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { setDetailedAccesses } from "../../../actions/accessInfos";
import { Store } from "../../../store";

function DetailedAccessesTableRow(props) {
    const [open, setOpen] = useState(false);
    const [openDialog, setOpenDialog] = useState(false);
    const [userDisconnect, setUserDisconnect] = useState(0);
    const classes = useStyles();

    const handleClickConfirmDialog = () => {
        console.log(userDisconnect);
        api.post(
            "api/updateLogin/forcelogout",
            {},
            {
                params: {
                    idLogin: userDisconnect,
                },
            }
        )
            .then((response) => {
                showSuccessToast("Logout realizado com sucesso!");
                setOpenDialog(false);
                api.get("api/filtroacesso/obteracessospordata", {
                    params: {
                        data: props.selectedDate,
                    },
                })
                    .then((response) => {
                        Store.dispatch(setDetailedAccesses(response.data));
                    })
                    .catch((error) => {
                        showErrorToast("Erro ao atualizar a tabela de logins");
                    });
            })
            .catch((error) => {
                showErrorToast("Ocorreu um erro ao forçar o logout");
            });
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    const handleClickButtonDisconnect = async (id) => {
        setUserDisconnect(id);
        setOpenDialog(true);
    };

    const handleClickButtonMessage = (
        codigoRecuperador,
        loginRecuperador,
        position
    ) => {
        props.onClickButtonMessage(
            props.groupName,
            codigoRecuperador,
            loginRecuperador,
            position
        );
    };

    return (
        <React.Fragment>
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Confirmação"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Deseja realmente forçar o logout? <br />
                        OBS: O logout do usuário será realizado no próximo ciclo
                        do SRC(30 segundos).
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={handleCloseDialog}
                        style={{
                            color: "#1A75BA",
                        }}
                        autoFocus
                    >
                        Cancelar
                    </Button>
                    <Button
                        onClick={handleClickConfirmDialog}
                        style={{
                            color: "#C91F1F",
                        }}
                    >
                        Confirmar
                    </Button>
                </DialogActions>
            </Dialog>
            <TableRow
                onClick={() => setOpen(!open)}
                style={{ backgroundColor: "#F4F7FA", cursor: "pointer" }}
            >
                <TableCell>
                    <IconButton aria-label="expand row" size="small">
                        {open ? (
                            <KeyboardArrowUpIcon />
                        ) : (
                            <KeyboardArrowDownIcon />
                        )}
                    </IconButton>
                    {props.groupName}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell
                    style={{ paddingBottom: 0, paddingTop: 0 }}
                    colSpan={6}
                >
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Table size="small" aria-label="logins">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Login</TableCell>
                                        <TableCell align="center">
                                            Horário Inicial
                                        </TableCell>
                                        <TableCell align="center">
                                            Horário Final
                                        </TableCell>
                                        <TableCell align="right"></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {props.row.map((usersRow) => (
                                        <TableRow key={usersRow.Id}>
                                            <TableCell
                                                component="th"
                                                scope="row"
                                            >
                                                {usersRow.LoginRecuperador}
                                            </TableCell>
                                            <TableCell align="center">
                                                {usersRow.HorarioLogin}
                                            </TableCell>
                                            <TableCell align="center">
                                                {usersRow.HorarioLogout}
                                            </TableCell>
                                            <TableCell align="right">
                                                {moment().format(
                                                    "DD/MM/YYYY"
                                                ) === props.selectedDate &&
                                                    usersRow.HorarioLogout ===
                                                        "" && (
                                                        <Button
                                                            className={
                                                                classes.actionButtons
                                                            }
                                                            style={{
                                                                backgroundColor:
                                                                    "#C91F1F",
                                                            }}
                                                            onClick={(value) =>
                                                                handleClickButtonDisconnect(
                                                                    usersRow.Id
                                                                )
                                                            }
                                                        >
                                                            <PowerSettingsNewIcon
                                                                style={{
                                                                    color:
                                                                        "white",
                                                                }}
                                                            />
                                                        </Button>
                                                    )}
                                                <Button
                                                    className={
                                                        classes.actionButtons
                                                    }
                                                    style={{
                                                        backgroundColor:
                                                            "#1A75BA",
                                                    }}
                                                    onClick={(value) => {
                                                        handleClickButtonMessage(
                                                            usersRow.CodigoRecuperador,
                                                            usersRow.LoginRecuperador,
                                                            {
                                                                X: value.pageX,
                                                                Y: value.pageY,
                                                            }
                                                        );
                                                    }}
                                                >
                                                    <LocalPostOfficeIcon
                                                        style={{
                                                            color: "white",
                                                        }}
                                                    />
                                                    {usersRow.Mensagens > 0 && (
                                                        <span
                                                            className={
                                                                classes.notificationBadge
                                                            }
                                                        >
                                                            {usersRow.Mensagens >
                                                            9
                                                                ? "9+"
                                                                : usersRow.Mensagens}
                                                        </span>
                                                    )}
                                                </Button>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

DetailedAccessesTableRow.propTypes = {
    row: PropTypes.arrayOf(
        PropTypes.shape({
            Id: PropTypes.number.isRequired,
            CodigoRecuperador: PropTypes.number.isRequired,
            LoginRecuperador: PropTypes.string.isRequired,
            HorarioLogin: PropTypes.string.isRequired,
            HorarioLogout: PropTypes.string.isRequired,
            Mensagens: PropTypes.number.isRequired,
        })
    ).isRequired,
    selectedDate: PropTypes.string.isRequired,
    groupName: PropTypes.string.isRequired,
    onClickButtonMessage: PropTypes.func.isRequired,
};

export default DetailedAccessesTableRow;
