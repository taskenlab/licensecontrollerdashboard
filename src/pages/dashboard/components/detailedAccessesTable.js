import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { PaperAccessesTable } from "../styles";
import DetailedAccessesTableRow from "./detailedAccessesTableRow";
import Chat from "./chat";
import api from "../../../services/api";
import { showErrorToast } from "../../../utils/toasty";
import { removeNotification } from "../../../utils/utils";
import {
    setOpenChat,
    setChatObject,
    setChatMessages,
} from "../../../actions/chat";

function DetailedAccessesTable({
    selectedDate,
    filteredAccesses,
    openChat,
    chatObject,
    dispatch,
}) {
    //timer
    useEffect(() => {
        const intervalId = setInterval(() => {
            if (openChat) {
                getMessages(chatObject.chatCodigoRecuperador).then(
                    (response) => {
                        dispatch(setChatMessages(response.data));
                    }
                );
            }
        }, 15000);

        return () => {
            clearInterval(intervalId);
        };
    }, [openChat]);

    const handleCloseClickChat = () => {
        dispatch(setOpenChat(false));
    };

    const handleClickButtonShowMessages = async (
        groupName,
        codigoRecuperador,
        loginRecuperador,
        position
    ) => {
        if (openChat) {
            dispatch(setOpenChat(false));
            if (chatObject.chatRecuperador === loginRecuperador) return;
        }

        return getMessages(codigoRecuperador)
            .then((response) => {
                removeNotification(
                    groupName,
                    codigoRecuperador,
                    filteredAccesses
                );
                dispatch(
                    setChatObject({
                        chatRecuperador: loginRecuperador,
                        chatPosition: position,
                        chatMessages: response.data,
                        chatCodigoRecuperador: codigoRecuperador,
                        chatGrupo: groupName,
                    })
                );
                dispatch(setOpenChat(true));
            })
            .catch();
    };

    const handleClickSendMessageChat = async (message) => {
        return api
            .post("api/mensagem/enviarmensagem", {
                mensagem: message,
                codigoRecuperador: chatObject.chatCodigoRecuperador,
            })
            .then((response) => {
                dispatch(setChatMessages(response.data));
            })
            .catch((error) => {
                showErrorToast("Ocorreu um erro ao enviar a mensagem");
                throw error;
            });
    };

    const getMessages = (codigoRecuperador) => {
        return api
            .get("api/mensagem/obtermensagensporrecuperadorV2", {
                params: {
                    recuperador: codigoRecuperador,
                },
            })
            .catch((error) => {
                showErrorToast("Ocorreu um erro ao obter mensagens");
                throw error;
            });
    };

    return (
        <React.Fragment>
            <Chat
                recuperador={chatObject.chatRecuperador}
                messages={chatObject.chatMessages}
                openChat={openChat}
                onCloseClick={handleCloseClickChat}
                onClickSendMessage={handleClickSendMessageChat}
                position={chatObject.chatPosition}
                codigoRecuperador={chatObject.chatCodigoRecuperador}
            />
            <TableContainer component={PaperAccessesTable}>
                <Table aria-label="collapsible table">
                    <TableHead>
                        <TableRow>
                            <TableCell
                                align="left"
                                style={{ paddingLeft: "45px" }}
                            >
                                Grupo
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {filteredAccesses &&
                            Object.keys(filteredAccesses).map((keyName) => (
                                <DetailedAccessesTableRow
                                    key={keyName}
                                    row={filteredAccesses[keyName]}
                                    groupName={keyName}
                                    selectedDate={selectedDate}
                                    onClickButtonMessage={
                                        handleClickButtonShowMessages
                                    }
                                />
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </React.Fragment>
    );
}

export default connect((state) => ({
    filteredAccesses: state.accessInfos.filteredDetailedAccesses,
    openChat: state.chat.openChat,
    chatObject: state.chat.chatObject,
    selectedDate: state.accessInfos.filter.selectedDate,
}))(DetailedAccessesTable);
