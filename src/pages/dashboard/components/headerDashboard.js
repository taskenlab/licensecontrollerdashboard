import React, { useState, useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";
import { useStyles } from "../styles";
import { Grid, CardMedia } from "@material-ui/core";
import "react-toastify/dist/ReactToastify.css";
import DescriptionIcon from "@material-ui/icons/Description";
import DateRangeIcon from "@material-ui/icons/DateRange";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Button from "@material-ui/core/Button";
import PublishIcon from "@material-ui/icons/Publish";
import api from "../../../services/api";
import { logout } from "../../../services/auth";
import moment from "moment";
import clsx from "clsx";
import { connect } from "react-redux";
import LicenceConfirmDialog from "./licenceConfirmDialog";
import { arrtipoLicenca } from "../../../utils/tipoLicenca";
import { showErrorToast } from "../../../utils/toasty";
import {
    setValorVisualizarLicenca,
    setStateLicenceDialog,
    setValorLicenca,
} from "../../../actions/licence";

function HeaderDashboard({ licenca, dispatch }) {
    const classes = useStyles();
    const history = useHistory();
    const inputFile = useRef(null);
    const [size, setSize] = useState([0, 0]);
    const [nomeUsuario, setNomeUsuario] = useState("");
    const [nomeAssessoria, setNomeAssessoria] = useState("");

    const handleClickLogOut = () => {
        logout();
        history.push("/");
    };

    const handleResize = () => {
        setSize([window.innerWidth, window.innerHeight]);
    };

    useEffect(() => {
        window && window.addEventListener("resize", handleResize);
        handleResize();
        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    useEffect(() => {
        api.get("api/licenca/obterInformacoes")
            .then((response) => {
                setNomeAssessoria(
                    response.data.NomeAssessoria ?? response.data
                );
                dispatch(
                    setValorLicenca({
                        quantidade: response.data.Quantidade ?? 0,
                        tipoLicenca: response.data.TipoLicenca ?? 0,
                        validade:
                            response.data.Validade ?? "Licença não cadastrada",
                    })
                );
            })
            .catch((error) => {
                showErrorToast();
            });

        api.get("api/usuario/obterNome")
            .then((response) => {
                setNomeUsuario(response.data);
            })
            .catch((error) => {
                showErrorToast();
            });
    }, [dispatch]);

    const handleClickUploadLicence = () => {
        inputFile.current.value = null;
        inputFile.current.click();
    };

    const handleChangeInputFile = (event) => {
        if (event.target.files.length > 0) {
            const reader = new FileReader();
            reader.onload = async (e) => {
                const text = e.target.result;
                await api
                    .post("api/licenca/lerLicenca", { hashLicenca: text })
                    .then((response) => {
                        dispatch(
                            setValorVisualizarLicenca({
                                quantidade: response.data.Quantidade,
                                tipoLicenca: response.data.TipoLicenca,
                                validade: response.data.Validade,
                                hash: text,
                            })
                        );
                        dispatch(setStateLicenceDialog(true));
                    })
                    .catch((error) => {
                        console.log(error);
                        showErrorToast(
                            "Ocorreu um erro ao carregar o arquivo de licença"
                        );
                    });
            };
            reader.readAsText(event.target.files[0]);
        }
    };

    return (
        <Grid>
            <LicenceConfirmDialog />
            <Grid container direction="row" className={classes.topLine}>
                <Grid item xs={4}></Grid>
                <Grid
                    container
                    item
                    xs={4}
                    direction="row"
                    justify="center"
                    style={{ display: "flex" }}
                >
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        className={classes.gridLogo}
                    >
                        <img
                            src={"LogoTasken_Preto.svg"}
                            className={classes.logo}
                            alt=""
                        />
                    </Grid>
                </Grid>
                <Grid
                    container
                    item
                    xs={4}
                    direction="row"
                    justify="flex-end"
                    style={{ display: "flex" }}
                >
                    <div className={classes.textoUsuario}>
                        Bem vindo(a), {nomeUsuario}!
                    </div>
                    <div
                        className={clsx(
                            classes.textoSair,
                            classes.textoUsuario
                        )}
                        onClick={handleClickLogOut}
                    >
                        Sair
                        <ExitToAppIcon
                            style={{
                                color: "white",
                                fontSize: "15px",
                                paddingLeft: "5px",
                            }}
                        />
                    </div>
                </Grid>
            </Grid>
            <CardMedia image={"imgfundo.jpg"} className={classes.imgHeader}>
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    style={{ display: "flex", width: "100%", height: "100%" }}
                >
                    <p className={classes.textotitulo}>
                        CONTROLE DE LICENÇAS SRC
                        <br />
                        {nomeAssessoria}
                    </p>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                        className={classes.imageInfoGrid}
                    >
                        <Grid
                            item
                            xs={3}
                            direction="row"
                            justify="flex-start"
                            alignItems="center"
                            className={clsx(
                                classes.griditeminfos,
                                classes.gridtextinfo
                            )}
                        >
                            <Grid
                                container
                                item
                                xs={3}
                                direction="row"
                                justify="flex-end"
                                style={{
                                    display: "flex",
                                    marginLeft: "3px",
                                    maxWidth: "25%",
                                }}
                            >
                                <Grid
                                    container
                                    direction="row"
                                    justify="center"
                                    alignItems="center"
                                    className={classes.gridcircle}
                                >
                                    <DescriptionIcon
                                        style={{ color: "white" }}
                                    />
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                item
                                xs={10}
                                direction="column"
                                className={classes.gridtextinfo}
                            >
                                <div className={classes.textoinfo}>
                                    Licenças liberadas
                                </div>
                                <div className={classes.textodadosinfo}>
                                    {licenca.tipoLicenca == 0
                                        ? licenca.quantidade
                                        : "∞"}
                                </div>
                            </Grid>
                        </Grid>
                        <Grid
                            item
                            xs={4}
                            direction="row"
                            justify="flex-start"
                            alignItems="center"
                            className={clsx(
                                classes.griditeminfos,
                                classes.griddatainfo
                            )}
                        >
                            <Grid item xs={1}>
                                {size && size[0] > 671 ? (
                                    <div
                                        className={classes.verticallinedata}
                                    ></div>
                                ) : (
                                    ""
                                )}
                            </Grid>
                            <Grid
                                container
                                item
                                xs={3}
                                direction="row"
                                justify="flex-end"
                                style={{
                                    display: "flex",
                                    marginLeft: "3px",
                                    maxWidth: "15%",
                                }}
                            >
                                <Grid
                                    container
                                    direction="row"
                                    justify="center"
                                    alignItems="center"
                                    className={classes.gridcircle}
                                >
                                    <DateRangeIcon style={{ color: "white" }} />
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                item
                                xs={8}
                                direction="column"
                                className={classes.gridtextinfo}
                            >
                                <div className={classes.textoinfo}>
                                    Data de Expiração do Arquivo
                                </div>
                                <div className={classes.textodadosinfo}>
                                    {moment(licenca.validade).isValid()
                                        ? moment(licenca.validade).format(
                                              "DD/MM/YYYY"
                                          )
                                        : licenca.validade}
                                </div>
                            </Grid>
                        </Grid>
                        <Grid
                            item
                            xs={2}
                            direction="row"
                            justify="flex-start"
                            alignItems="center"
                            className={classes.griditeminfos}
                            style={{ minWidth: "140px" }}
                        >
                            <Grid item xs={1}>
                                {size && size[0] > 956 ? (
                                    <div
                                        className={classes.verticallinedata}
                                    ></div>
                                ) : (
                                    ""
                                )}
                            </Grid>
                            <Grid
                                item
                                xs={11}
                                direction="column"
                                className={classes.gridtextinfo}
                            >
                                <div className={classes.titulolicenca}>
                                    Licença
                                </div>
                                <div className={classes.tipolicenca}>
                                    {arrtipoLicenca[licenca.tipoLicenca]}
                                </div>
                            </Grid>
                        </Grid>
                        <Grid
                            item
                            xs={3}
                            direction="row"
                            justify="center"
                            alignItems="center"
                            className={clsx(
                                classes.griditeminfos,
                                classes.gridtextinfo
                            )}
                        >
                            <Grid
                                container
                                item
                                xs={12}
                                direction="column"
                                className={classes.gridtextinfo}
                            >
                                <input
                                    type="file"
                                    id="file"
                                    ref={inputFile}
                                    style={{ display: "none" }}
                                    onChange={handleChangeInputFile}
                                    accept=".srckey"
                                />
                                <Button
                                    variant="contained"
                                    className={classes.buttonArqLicenca}
                                    onClick={handleClickUploadLicence}
                                >
                                    <PublishIcon
                                        style={{
                                            color: "white",
                                            marginRight: 10,
                                        }}
                                    />
                                    Subir Arquivo de Licença
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </CardMedia>
        </Grid>
    );
}

export default connect((state) => ({ licenca: state.licence.licenca }))(
    HeaderDashboard
);
