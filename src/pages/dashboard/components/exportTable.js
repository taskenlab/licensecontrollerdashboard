import React from "react";
import { connect } from "react-redux";

function ExportTable({ filteredAccesses, selectedDate }) {
    return (
        <React.Fragment>
            <table id="acessos-xls" style={{ display: "none" }}>
                <tbody>
                    <table>
                        <tr>
                            <th
                                colspan="3"
                                rowspan="3"
                                style={{
                                    textAlign: "center",
                                    backgroundColor: "#1A70BA",
                                    color: "white",
                                }}
                            >
                                Relatório de acessos por dia
                            </th>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Data: </td>
                            <td>{selectedDate}</td>
                        </tr>
                        <tr>
                            <td>Total: </td>
                            <td>
                                {Object.keys(filteredAccesses).length > 0
                                    ? Object.keys(filteredAccesses)
                                          .map((group) => {
                                              return filteredAccesses[group]
                                                  .length;
                                          })
                                          .reduce(
                                              (accumulator, currentValue) =>
                                                  accumulator + currentValue
                                          )
                                    : 0}
                            </td>
                        </tr>
                        <tr></tr>
                        {Object.keys(filteredAccesses).map((group) => {
                            return (
                                <React.Fragment>
                                    <tr>
                                        <td
                                            colspan="3"
                                            style={{
                                                textAlign: "center",
                                                backgroundColor: "#F4F7FA",
                                            }}
                                        >
                                            {group}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>LOGIN</td>
                                        <td>HORÁRIO LOGIN</td>
                                        <td>HORÁRIO LOGOUT</td>
                                    </tr>
                                    {filteredAccesses[group].map((infos) => {
                                        return (
                                            <tr>
                                                <td>
                                                    {infos.LoginRecuperador}
                                                </td>
                                                <td
                                                    style={{
                                                        textAlign: "center",
                                                    }}
                                                >
                                                    {infos.HorarioLogin}
                                                </td>
                                                <td
                                                    style={{
                                                        textAlign: "center",
                                                    }}
                                                >
                                                    {infos.HorarioLogout}
                                                </td>
                                            </tr>
                                        );
                                    })}
                                    <tr></tr>
                                </React.Fragment>
                            );
                        })}
                    </table>
                </tbody>
            </table>
        </React.Fragment>
    );
}

export default connect((state) => ({
    filteredAccesses: state.accessInfos.filteredDetailedAccesses,
    selectedDate: state.accessInfos.filter.selectedDate,
}))(ExportTable);
