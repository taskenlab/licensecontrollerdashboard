import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { connect } from "react-redux";
import { arrtipoLicenca } from "../../../utils/tipoLicenca";
import moment from "moment";
import api from "../../../services/api";
import { showErrorToast, showSuccessToast } from "../../../utils/toasty";
import {
    setStateLicenceDialog,
    setValorLicenca,
} from "../../../actions/licence";

function LicenceConfirmDialog({ licencaConfirmacao, open, dispatch }) {
    const handleClose = () => {
        dispatch(setStateLicenceDialog(false));
    };

    const handleConfirm = () => {
        api.post("api/licenca/adicionarLicenca", {
            hashLicenca: licencaConfirmacao.hash,
        })
            .then((response) => {
                dispatch(
                    setValorLicenca({
                        validade: response.data.Validade,
                        tipoLicenca: response.data.TipoLicenca,
                        quantidade: response.data.Quantidade,
                    })
                );
                showSuccessToast("Licença carregada com sucesso!");
            })
            .catch((error) => {
                showErrorToast("Ocorreu um erro ao carregar a licença");
            });
        dispatch(setStateLicenceDialog(false));
    };

    return (
        <React.Fragment>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Deseja realmente substituir a licença atual?
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Quantidade: {licencaConfirmacao.quantidade} <br />
                        Expiração:{" "}
                        {moment(licencaConfirmacao.validade).format(
                            "DD/MM/YYYY"
                        )}{" "}
                        <br />
                        Tipo Licenca:{" "}
                        {
                            arrtipoLicenca[licencaConfirmacao.tipoLicenca - 1]
                        }{" "}
                        <br />
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={handleClose}
                        autoFocus
                        style={{ color: "#1A75BA" }}
                    >
                        Cancelar
                    </Button>
                    <Button
                        onClick={handleConfirm}
                        style={{ color: "#C91F1F" }}
                    >
                        Confirmar
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default connect((state) => ({
    licencaConfirmacao: state.licence.licencaConfirmacao,
    open: state.licence.open,
}))(LicenceConfirmDialog);
