import { withStyles, makeStyles } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";

export const MessageTextBox = withStyles({
    root: {
        width: "85%",
        marginBottom: "5px",
        height: "80px",
        "& label.Mui-focused": {
            color: "#1A70BA",
        },
        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "#CED4DA",
                borderRadius: "2px !important",
                height: "80px",
            },
            "&:hover fieldset": {
                borderColor: "black",
            },
            "&.Mui-focused fieldset": {
                borderColor: "#1A70BA",
            },
        },
    },
})(TextField);

export const useStyles = makeStyles((theme) => ({
    root: {
        width: "350px",
        height: "600px",
        backgroundColor: "white",
        border: "1px solid #DBE5ED",
        position: "absolute",
        zIndex: "999",
        fontFamily: "Source Sans Pro, sans-serif",
    },
    header: {
        width: "100%",
        height: "85px",
    },
    content: {
        width: "100%",
        height: "350px",
    },
    footer: {
        width: "100%",
        height: "150px",
        borderTop: "1px solid #DBE5ED",
    },
    buttonEnviarMensagem: {
        width: "85%",
        backgroundColor: "#1A75BA",
        borderRadius: 0,
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "13px",
        lineHeight: "16px",
        color: "#FFFFFF",
        textTransform: "none",
        height: "50px",
        alignSelf: "center",
        "&:hover": {
            backgroundColor: "#1A70BA",
            borderColor: "#1A70BA",
        },
    },
    headerText: {
        fontFamily: "Source Sans Pro, sans-serif",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "20px",
        lineHeight: "25px",
        color: "#1A75BA",
    },
    headerMessageIcon: {
        marginLeft: "20px",
        marginRight: "5px",
        fontSize: "25px",
    },
    closeIcon: {
        color: "black",
        fontSize: "20px",
        marginTop: "15px",
        marginRight: "15px",
        cursor: "pointer",
    },
    horizontalHeaderLine: {
        width: "90%",
        border: "1px solid #DBE5ED",
        marginTop: "0px",
        marginBottom: "0px",
    },
}));
