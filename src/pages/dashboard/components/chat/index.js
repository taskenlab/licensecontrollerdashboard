import React, { useEffect, useState, useRef } from "react";
import { useStyles, MessageTextBox } from "./styles";
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import CloseIcon from "@material-ui/icons/Close";
import { Grid, Button } from "@material-ui/core";
import clsx from "clsx";
import PropTypes from "prop-types";
import MessageComponent from "./components/message";
import { Scrollbars } from "react-custom-scrollbars";
import Grow from "@material-ui/core/Grow";
import api from "../../../../services/api";

function Chat(props) {
    const [message, setMessage] = useState("");
    const scrollbar = useRef(null);
    const classes = useStyles();

    useEffect(() => {
        if (props.openChat) {
            setMessageAsSaw();
            scrollbar.current.scrollTop(scrollbar.current.getScrollHeight());
        }
    }, [props.messages]);

    const handleCloseClick = () => {
        props.onCloseClick();
    };

    const handleClickSendMessage = () => {
        let messageToSend = message;
        setMessage("");
        props
            .onClickSendMessage(messageToSend)
            .then((ret) => {
                setMessage("");
            })
            .catch((error) => setMessage(messageToSend));
    };

    const setMessageAsSaw = () => {
        api.post(
            "api/mensagem/definirrespostavisualizada",
            {},
            { params: { recuperador: props.codigoRecuperador } }
        ).catch();
    };

    const handleMessageChange = (event) => {
        setMessage(event.target.value);
    };

    const handleOpenChat = () => {
        setMessageAsSaw();
        scrollbar.current.scrollTop(scrollbar.current.getScrollHeight());
    };

    return (
        <Grow
            in={props.openChat}
            onEnter={handleOpenChat}
            style={{ transformOrigin: "100% 100% 0" }}
        >
            <Grid
                container
                direction="column"
                className={classes.root}
                style={{
                    ...props.style,
                    left: props.openChat
                        ? props.position.X - 350 + "px"
                        : "0px",
                    top: props.openChat ? props.position.Y - 600 + "px" : "0px",
                }}
            >
                <Grid
                    container
                    className={clsx(classes.header, classes.headerText)}
                >
                    <Grid
                        item
                        xs={10}
                        direction="row"
                        alignItems="center"
                        style={{ display: "flex" }}
                    >
                        <QuestionAnswerIcon
                            className={classes.headerMessageIcon}
                        />
                        Mensagens de {props.recuperador}
                    </Grid>
                    <Grid
                        item
                        xs={2}
                        container
                        direction="row"
                        justify="flex-end"
                        style={{ height: "100%" }}
                    >
                        <CloseIcon
                            className={classes.closeIcon}
                            onClick={handleCloseClick}
                        />
                    </Grid>
                </Grid>
                <hr className={classes.horizontalHeaderLine} />
                <Grid
                    container
                    diretion="row"
                    justify="center"
                    alignContent="center"
                    className={classes.content}
                >
                    <nav
                        style={{
                            width: "100%",
                            height: "95%",
                            justifyContent: "center",
                        }}
                    >
                        <Scrollbars style={{ height: "100%" }} ref={scrollbar}>
                            {props.messages.map((message, i) => {
                                return (
                                    <MessageComponent
                                        key={i}
                                        message={message.Mensagem}
                                        sendDate={message.Data}
                                        backgroundColor={
                                            message.Mine ? "#F4F7FA" : "white"
                                        }
                                        from={
                                            message.Mine
                                                ? "Você"
                                                : props.recuperador
                                        }
                                    />
                                );
                            })}
                        </Scrollbars>
                    </nav>
                </Grid>
                <Grid
                    container
                    diretion="row"
                    justify="center"
                    alignContent="center"
                    className={classes.footer}
                >
                    <MessageTextBox
                        variant="outlined"
                        placeholder="Escreva sua mensagem"
                        multiline={true}
                        autoFocus={true}
                        rowsMax={3}
                        value={message}
                        onChange={handleMessageChange}
                    />
                    <Button
                        variant="contained"
                        className={classes.buttonEnviarMensagem}
                        onClick={handleClickSendMessage}
                    >
                        Enviar Mensagem
                    </Button>
                </Grid>
            </Grid>
        </Grow>
    );
}

Chat.propTypes = {
    onClickSendMessage: PropTypes.func.isRequired,
    onCloseClick: PropTypes.func.isRequired,
    openChat: PropTypes.bool.isRequired,
    codigoRecuperador: PropTypes.number.isRequired,
    recuperador: PropTypes.string.isRequired,
    messages: PropTypes.arrayOf(
        PropTypes.shape({
            Mensagem: PropTypes.string.isRequired,
            Data: PropTypes.string.isRequired,
            Mine: PropTypes.bool.isRequired,
        })
    ).isRequired,
    position: PropTypes.objectOf(
        PropTypes.shape({
            X: PropTypes.number.isRequired,
            Y: PropTypes.number.isRequired,
        })
    ).isRequired,
};

export default Chat;
