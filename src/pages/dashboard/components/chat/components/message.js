import React from "react";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";

function MessageComponent(props) {
    const { message, sendDate, from, backgroundColor } = props;
    return (
        <Grid
            container
            direction="row"
            justify="center"
            style={{ marginBottom: "20px" }}
        >
            <Grid
                style={{
                    backgroundColor,
                    borderRadius: "6px",
                    width: "85%",
                }}
            >
                <Grid
                    style={{
                        margin: "6px 6px 6px 6px",
                        wordWrap: "break-word",
                        textAlign: from === "Você" ? "right" : "left",
                    }}
                >
                    <b>{from} </b>
                    {sendDate}
                    <div style={{ marginTop: "5px" }}>{message}</div>
                </Grid>
            </Grid>
        </Grid>
    );
}

MessageComponent.propTypes = {
    message: PropTypes.string.isRequired,
    sendDate: PropTypes.string.isRequired,
    from: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string.isRequired,
};

export default MessageComponent;
