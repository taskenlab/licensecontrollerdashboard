import axios from "axios";
import { getToken, logout } from "./auth";

const api = axios.create({ baseURL: process.env.REACT_APP_LICENCE_API });

api.interceptors.request.use(async (config) => {
    const token = getToken();
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});

api.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error.response.status === 401) {
            logout();
            window.location.reload(); //forçando reload para voltar a tela de login
        }
        return error;
    }
);

export default api;
